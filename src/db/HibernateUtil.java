package db;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

	private static final SessionFactory sessionFactory;
	 
	static {
		try {
//			sessionFactory = new Configuration().configure().buildSessionFactory();
			
			  AnnotationConfiguration aconf = new AnnotationConfiguration()
		      .addAnnotatedClass(Accounts.class)
		      .addAnnotatedClass(AlarmSimple.class);
		      
			  Configuration conf = aconf.configure();
			
			sessionFactory = conf.buildSessionFactory();
			
		} 
		catch (ExceptionInInitializerError ex) {
			System.out.println("Initial SessionFactory creation failed: " + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	 
	/**
	* Gets hiberante session factory that was initialized at application startup.
	*
	* @return hibernate session factory
	*/
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}