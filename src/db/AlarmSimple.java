package db;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "alarm_simple")
public class AlarmSimple {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name="alarm_dt")
//	@Temporal(value=TemporalType.DATE)
	private Date alatmDT;
	
	@Column(name="alarm_text")
	private String alarmText;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@Column(name="user_id")
	private Long UserId;

	public Date getAlatmDT() {
		return alatmDT;
	}

	public void setAlatmDT(Date alatmDT) {
		this.alatmDT = alatmDT;
	}

	public String getAlarmText() {
		return alarmText;
	}

	public void setAlarmText(String alarmText) {
		this.alarmText = alarmText;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Long getUserId() {
		return UserId;
	}

	public void setUserId(Long userId) {
		UserId = userId;
	}

	public Long getId() {
		return id;
	}
	
	
}
