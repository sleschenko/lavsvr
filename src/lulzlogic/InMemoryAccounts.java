package lulzlogic;

import java.util.HashMap;
import java.util.Map;

public class InMemoryAccounts implements IAccountManager {

	private Map<String, AccountData> accounts;

	public InMemoryAccounts(){
		accounts = new HashMap<String, AccountData>();
		initAccountsData();
	}

	@Override
	public boolean isValidUser(String login, String pwd){
		
		if(accounts.containsKey(login)){
			if(accounts.get(login).getPwd().equals(pwd)){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public long getUserId(String login, String pwd) {
		if(accounts.containsKey(login)){
			if(accounts.get(login).getPwd().equals(pwd)){
				return accounts.get(login).getId();
			}
		}
		return -1;
	}

	private void initAccountsData() {
		accounts.put("bream", new AccountData("bream","ass", 0));
		accounts.put("vano", new AccountData("vano","asd", 1));
		accounts.put("dmytro", new AccountData("dmytro","zxc", 2));
	}
}
