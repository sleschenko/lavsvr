package lulzlogic;

public interface IAccountManager {

	boolean isValidUser(String login, String pwd);
	long getUserId(String login, String pwd);
}
