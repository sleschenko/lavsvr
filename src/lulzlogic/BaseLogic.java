package lulzlogic;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class BaseLogic {

	private static final String RES_FAIL = "{\"resp\":\"fail\"}";
	private static final Logger log;
	
	private static JSONObject failResult;
	private static IAccountManager accounts;
	static {
		log = Logger.getLogger(BaseLogic.class.getName());
		accounts = new DBAccounts();
		try {
			failResult = new JSONObject(RES_FAIL);
		} catch (JSONException e) {
			log.fatal("application init fail!", e);
		}
	}
	
	public static String dataCommunication(String inData){
		log.trace(inData);

		String outData = RES_FAIL;
		JSONObject outJSON = failResult;
		JSONObject jsonMessage = createJSONObject(inData);
		if(jsonMessage != null){
			try {
				outJSON = processRequest(jsonMessage);
			} catch (JSONException e) {
				log.error("Exception: json message " + jsonMessage + " outJSON " + outJSON, e);
			}
			
			if(outJSON == null){
				outJSON = failResult;
			}
		}
		outData = outJSON.toString();
		return outData;
	}
	
	private static JSONObject createJSONObject(String jsonBody){
		JSONObject resultJson = null;
		try {
			resultJson = new JSONObject(jsonBody);
		} catch (JSONException e) {
			log.error(e);
		}
		return resultJson;
	}
	
	// verify request structure, login user and if OK - call parse command method
	private static JSONObject processRequest(JSONObject inJSON) throws JSONException{
		JSONObject outJSON = new JSONObject();
		boolean isGeneralSectionOK = false;
		String loginData = "";
		String commandData = "";
		String paramData = "";
		
		// verify and get general section data
		if(inJSON.has("account")){
			loginData = inJSON.getString("account");
			if(inJSON.has("command")){
				commandData = inJSON.getString("command");
				if(inJSON.has("data")){
					paramData = inJSON.getString("data");
					isGeneralSectionOK = true;
					outJSON.put("res", "ok");
				}
			}
		}
		if(isGeneralSectionOK){
			long userId = login(loginData); 
			if(userId > 0){
				outJSON.put("login", "ok");
				String response = runCommand(userId, commandData, paramData);
				outJSON.put("data", response);
			} else {
				outJSON.put("login", "fail");
				outJSON.put("data", "fail");
			}
				
		} else {
			outJSON.put("res", "fail");
		}
		return outJSON;
	}

	private static String runCommand(long userId, String commandData, String paramData) {
		String runResult = "command not found";
		if(commandData.equals("get_weather")){
			runResult = getWeather(paramData);
		}
		if(commandData.equals("get_say")){
			runResult = getSay(paramData);
		}
		if(commandData.equals("get_alarm_simple")){
			runResult = getAlarmSimple(userId, paramData);
		}
		return runResult;
	}

	private static String getAlarmSimple(long userId, String paramData) {
		return new Alarm1().getAlarmList(userId);
	}

	private static String getSay(String paramData) {
		return new SpeakManager().getSpeakContent(paramData);
	}

	private static String getWeather(String paramData) {
		// TODO Auto-generated method stub
		return "weather hujovaja...";
	}

	private static long login(String loginData) throws JSONException {
		String login = "";
		String pwd = "";
		
		JSONObject data = new JSONObject(loginData);
		
		if (data.has("login")){
			login = data.getString("login");
		}
		if (data.has("pwd")){
			pwd = data.getString("pwd");
		}
		if(login.isEmpty() || pwd.isEmpty())return 0;
		return accounts.getUserId(login, pwd);
	}
}
