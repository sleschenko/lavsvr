package lulzlogic;

public class AccountData {
	private String login;
	private String pwd;
	private int id;

	public AccountData(String login, String pwd, int id) {
		super();
		this.login = login;
		this.pwd = pwd;
		this.id = id;
	}

	public String getLogin() {
		return login;
	}
	private void setLogin(String login) {
		this.login = login;
	}
	public String getPwd() {
		return pwd;
	}
	private void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getId() {
		return id;
	}
	private void setId(int id) {
		this.id = id;
	}
}
