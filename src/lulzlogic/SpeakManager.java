package lulzlogic;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class SpeakManager {
	
	private static final String CMD_TEXT = "cmd";
	
	public String getSpeakContent(String paramData) {
		
		JSONObject result = new JSONObject();
		try {
			result.put(CMD_TEXT, "get_say");
			result.put("text", "Hello from java service!");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result.toString();
	}
}
