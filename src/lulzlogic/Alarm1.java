package lulzlogic;

import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Session;

import db.AlarmSimple;
import db.HibernateUtil;

public class Alarm1 {
	private static final Logger log = Logger.getLogger(DBAccounts.class.getName());

	public String getAlarmList(long userId) {

		String query = String.format("from AlarmSimple where UserId = '%s'", userId);
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<AlarmSimple> alarms = (List<AlarmSimple>) session.createQuery(query).list();
		
		JSONObject jsonResult = new JSONObject();
		//StringBuilder jsonAlarmList = new StringBuilder("["); 
		JSONArray jsonAlarmList = new JSONArray(); 
		try {
			jsonResult.put("cmd", "get_alarm_simple");
			
			for(AlarmSimple alarm : alarms){
				JSONObject jsonAlarm = new JSONObject();
				jsonAlarm.put("id", alarm.getId());
				jsonAlarm.put("dt", alarm.getAlatmDT());
				jsonAlarm.put("text", alarm.getAlarmText());

				log.info(alarm.getAlatmDT().toString());
				
				jsonAlarmList.put(jsonAlarm);
				log.info(jsonAlarm.toString());
			}
			jsonResult.put("alarms", jsonAlarmList);
		} catch (JSONException e) {
			log.error("creation response error for user: " + userId, e);
		}
		return jsonResult.toString();
	}		

}
