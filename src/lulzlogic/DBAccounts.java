package lulzlogic;

import org.apache.log4j.Logger;
import org.hibernate.Session;

import db.Accounts;
import db.HibernateUtil;

public class DBAccounts implements IAccountManager{

	private static final Logger log = Logger.getLogger(DBAccounts.class.getName());
	
	@Override
	@Deprecated
	public boolean isValidUser(String login, String pwd) {
		if(getUserId(login, pwd) > 0)
			return true;
		return false;
	}

	@Override
	public long getUserId(String login, String pwd) {
		long userId = 0;
		String query = String.format("from Accounts where login = '%s' and pass = '%s'", login, pwd);
		Session session = HibernateUtil.getSessionFactory().openSession();
		Accounts acc = (Accounts) session.createQuery(query).uniqueResult();
		
		if(acc != null){
			userId = acc.getId();
		} else {
			log.warn("user not found: " + login + ":" + pwd);
		}
		return userId;
	}
}
