package lulzservice;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

@Path("/userdata")
public class UserData {

  // Allows to insert contextual objects into the class, 
  // e.g. ServletContext, Request, Response, UriInfo
  @Context
  UriInfo uriInfo;
  @Context
  Request request;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public String getDef() {
    return java.util.UUID.randomUUID().toString(); 
  }

  @Path("dc")
  @POST
  @Produces(MediaType.APPLICATION_JSON)
  public String dataCommunication(String inData ){
	    return lulzlogic.BaseLogic.dataCommunication(inData);
  }

// test method
  @GET
  @Path("help")
  @Produces(MediaType.APPLICATION_JSON)
  public String getHelp( @DefaultValue("{\"null\"}") @QueryParam("indata") String inData){
	  return lulzlogic.BaseLogic.dataCommunication(inData);
  }
    
} 
